﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RainbowSixSiegePro.Models
{
    public class Operator
    {
        public string PicturePath { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
    }
}
