﻿using RainbowSixSiegePro.Models;
using RainbowSixSiegePro.Pages;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RainbowSixSiegePro
{
    public partial class App : Application
    {
        public List<Operator> Operators { get; set; }

        public App()
        {
            InitializeComponent();
            Operators = new List<Operator>();
            InitializeOperators();
            MainPage = new NavigationPage(new OperatorPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        public void AddOperatorToList(Operator @operator)
        {
            Operators.Add(@operator);
        }


        #region Operators
        private void InitializeOperators()
        {

            Operators.Add(new Operator
            {
                Name = "Attacker - Ash",
                PicturePath = "https://vignette.wikia.nocookie.net/rainbowsix/images/d/d7/Ash_Icon_-_Standard.png/revision/latest?cb=20151222045522",
                Details = "Eliza \"Ash\" Cohen is an Attacking Operator featured in Tom Clancy's Rainbow Six Siege. She is the acting leader of Forward Operations in Rainbow."
            });

            Operators.Add(new Operator
            {
                Name = "Attacker - Twitch",
                PicturePath = "https://vignette.wikia.nocookie.net/rainbowsix/images/4/47/Twitch_Badge_New_2.png/revision/latest?cb=20151222045527",
                Details = "Emmanuelle \"Twitch\" Pichon is an Attacking Operator featured in Tom Clancy's Rainbow Six Siege."
            });

            Operators.Add(new Operator
            {
                Name = "Attacker - Nokk",
                PicturePath = "https://vignette.wikia.nocookie.net/rainbowsix/images/2/2e/Nokk.png/revision/latest?cb=20190523003359",
                Details = "Nøkk is an Attacking Operator featured in Tom Clancy's Rainbow Six Siege. She was introduced in the Operation Phantom Sight expansion alongside Warden."
            });

            Operators.Add(new Operator
            {
                Name = "Defender - Lesion",
                PicturePath = "https://vignette.wikia.nocookie.net/rainbowsix/images/d/d2/R6S-Lesion.png/revision/latest?cb=20170828194022",
                Details = "Liu Tze Long, codenamed Lesion, is a Defending Operator featured in Tom Clancy's Rainbow Six Siege, introduced in the Operation Blood Orchid expansion alongside Ying and Ela"
            });

            Operators.Add(new Operator
            {
                Name = "Defender - Doc",
                PicturePath = "https://vignette.wikia.nocookie.net/rainbowsix/images/8/8f/Doc_Badge_2.png/revision/latest?cb=20151222045524",
                Details = "Gustave \"Doc\" Kateb is a Defending Operator featured in Tom Clancy's Rainbow Six Siege"
            });

            Operators.Add(new Operator
            {
                Name = "Defender - Caveira",
                PicturePath = "https://vignette.wikia.nocookie.net/rainbowsix/images/e/ef/R6S-badge-caveira.png/revision/latest?cb=20160802150554",
                Details = "Taina \"Caveira\" Pereira is a Defending Operator featured in Tom Clancy's Rainbow Six Siege. She was introduced in the Operation Skull Rain expansion alongside Capitão"
            });
        }

        #endregion
    }
}
