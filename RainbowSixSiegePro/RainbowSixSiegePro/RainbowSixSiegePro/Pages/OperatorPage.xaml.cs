﻿using RainbowSixSiegePro.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RainbowSixSiegePro.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OperatorPage : ContentPage
    {
        ObservableCollection<Operator> operators = new ObservableCollection<Operator>();
        public ObservableCollection<Operator> Operators { get { return operators; } }

        public OperatorPage()
        {
            InitializeComponent();
            OperatorsView.ItemsSource = operators;

            BindingContext = Operators;
            OnPropertyChanged(nameof(Operators));
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (operators.Count != 0)
                operators.Clear();

            RefreshOperators();
        }

        public void RefreshOperators()
        {
            var ops = ((App)App.Current).Operators;

            foreach (var op in ops)
                operators.Add(op);
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            var toDelete = Operators.FirstOrDefault(x => x.Name == mi.CommandParameter.ToString());

            Operators.Remove(toDelete);
            OnPropertyChanged(nameof(Operators));
        }

        private void OperatorsView_Refreshing(object sender, EventArgs e)
        {
            OperatorsView.IsRefreshing = true;

            Operators.Clear();
            RefreshOperators();
            OnPropertyChanged(nameof(Operators));

            OperatorsView.IsRefreshing = false;
        }

        private async void ViewCell_Tapped(object sender, ItemTappedEventArgs e)
        {
            var op = e.Item as Operator;
            await Navigation.PushAsync(new OperatorDetailPage(op));
        }

        private async void ToolbarItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddOperatorPage());
        }
    }
}