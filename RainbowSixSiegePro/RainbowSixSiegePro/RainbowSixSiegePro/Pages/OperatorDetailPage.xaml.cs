﻿using RainbowSixSiegePro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RainbowSixSiegePro.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OperatorDetailPage : ContentPage
    {
        public Operator Operator{ get; set; }

        public OperatorDetailPage(Operator @operator)
        {
            Operator = @operator;
            
            InitializeComponent();

            BindingContext = Operator;
        }
    }
}