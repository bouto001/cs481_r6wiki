﻿using RainbowSixSiegePro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RainbowSixSiegePro.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddOperatorPage : ContentPage
    {
        public Operator Operator { get; set; }


        public AddOperatorPage()
        {
            InitializeComponent();

            Operator = new Operator();

            BindingContext = this;
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            var newOp = new Operator
            {
                Name = Operator.Name,
                PicturePath = Operator.PicturePath,
                Details = Operator.Details,
            };

            ((App)App.Current).AddOperatorToList(newOp);
            
            await Navigation.PopAsync();
        }
    }
}